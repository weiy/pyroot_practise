# This is a project on how to use python to draw root plots

## Using python

1. setupathena is needed. python 3.6.8 is broken for cern root

1. prompt: `TBrowser`

   ```
   >>> import ROOT
   >>> br = ROOT.TBrowser()
   ```

1. Read ntuple

   - python

      ```
      >>> import ROOT
      >>> file = ROOT.TFile.Open("mu_scan_0902_redSys_wMCSTAT_noVBF.root")
      >>> file.ls()
      >>> tree = file.physics
      >>> tree.Print()
      ```

   - python compare c++

      - python
         ```
         import ROOT

         file = ROOT.TFile.Open("data_file.root")
         tree = file.data_tree

         for entry in tree:
         print(entry.eta)
         ```

      - c++

         ```
         TFile* file = TFile::Open("data_file.root");

         TTreeReader data_reader("data_tree", file);
         TTreeReaderValue<double> eta(data_reader, "eta");

         while (data_reader.Next()) {
            std::cout << *eta << std::endl;
         }
         ```
