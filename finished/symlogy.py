import numpy
from matplotlib import pyplot

# Enable interactive mode
pyplot.ion()

# Draw the grid lines
pyplot.grid(True)

x  = [1,    2,  3,  4,  5,  6,  7,  8,  9, 10]
y1 = [ 1,  2,  3, 4, 5, 6, 7, 8, 9, 10]
y2 = [-1,-2,-3,-4,-5,-6,-7,-8,-9,-10]
# Numbers from -50 to 50, with 0.1 as step

# Plots a simple linear function 'f(x) = x'
pyplot.plot(x,y1)
# Plots 'sin(x)'
pyplot.plot(x,y2)

# 'linear' is the default mode, so this next line is redundant:
pyplot.yscale('symlog')

#plot.show()
pyplot.savefig("symlog.png")
