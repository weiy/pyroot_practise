import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')

x1 = np.random.normal(0, 10, 100)
x2 = np.random.normal(-2, 10, 100)
x3 = np.random.normal(3, 10, 100)

def Convert(lst): 
    return [ -i for i in lst ]

x4 = Convert(x3)

kwargs = dict(histtype='stepfilled', alpha=0.3, density=True, bins=5, ec="k")

plt.hist(x1, **kwargs)
#plt.hist(x2, **kwargs)
#plt.hist(x3, **kwargs);
plt.hist(x4, **kwargs);

print(x4)

plt.yscale('symlog')
plt.ylim(-0.1, 0.1)
plt.xlim(-30, 30)
#plt.show()
plt.savefig("symlogy2.png")
