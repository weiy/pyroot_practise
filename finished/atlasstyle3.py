import ROOT
import atlasplots as aplt
aplt.set_atlas_style()

#ROOT.gROOT.LoadMacro("AtlasStyle.C")
#ROOT.SetAtlasStyle()

canv = ROOT.TCanvas("canv", "canv", 800, 600)
frame = ROOT.TH1F("frame", "AtlasStyle", 1, 0, 1)
frame.SetTitle("AtlasStyle")
frame.GetXaxis().SetTitle("X [GeV]")
frame.GetYaxis().SetTitle("Events")
frame.GetXaxis().SetLimits(0, 100)
frame.GetYaxis().SetLimits(0, 10)
frame.SetTitleSize(0.1,"t")

#ROOT.gROOT.GetStyle("ATLAS").SetTitleFontSize(0.05)
#ROOT.gROOT.GetStyle("ATLAS").SetOptTitle(1)
#ROOT.gROOT.GetStyle("ATLAS").SetTitleBorderSize(0)
#ROOT.gROOT.GetStyle("ATLAS").SetTitleStyle(0)
#ROOT.gROOT.GetStyle("ATLAS").SetTitleFont(42)

#ROOT.gStyle.SetTitleFontSize(0.04): work if not atlasstyle
#ROOT.AtlasStyle.SetTitleFontSize(0.04): doesn't work
#frame.Draw("AXIS")
frame.Draw("")
print(frame.GetTitleSize())
print(frame.GetXaxis().GetTitleSize())
print(ROOT.gROOT.GetStyle("ATLAS").GetTitleFontSize())
print(ROOT.gROOT.GetStyle("ATLAS").GetOptTitle())
aplt.atlas_label(0.2, 0.83, "Internal")

canv.SaveAs("figure.png")

