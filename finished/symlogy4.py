import warnings
warnings.filterwarnings("ignore")

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import matplotlib.font_manager
from matplotlib.ticker import Locator
from matplotlib.pyplot import figure, gca

import matplotlib.image as image
from matplotlib.offsetbox import OffsetImage,AnchoredOffsetbox

#im = image.imread('arrow.png')


#import ROOT
#import atlasplots as aplt
#aplt.set_atlas_style()

figure(num=None, figsize=(5, 3.75), dpi=200, facecolor='w', edgecolor='k')

matplotlib.font_manager.findSystemFonts(fontpaths=None, fontext='ttf')


x = range(7)
negative_data = [-1,-4,-3,-2,-6,-2,-8]
positive_data = [4,6,3,5,7,9,10]
positive_data1 = [2,1,1,2,5,3,6]

fig = plt.figure()
ax = plt.subplot(111) #needed
#color
#green: #157477
#red: #760403
#orange: #F8450C

ax.bar(x, positive_data, width=1, edgecolor='#760403', linewidth =2, fill=False,linestyle="-", label="VBF")
ax.bar(x, negative_data, width=1, edgecolor='#157477', linewidth =2, fill=False,linestyle=":", label="ggF")
ax.bar(x, positive_data1, width=1, edgecolor='#F8450C', linewidth =2, fill=False,linestyle="--", label="qqZZ")


ax.minorticks_on()
ax.tick_params(direction='in', length=7, which='major', labelsize=14)
ax.tick_params(direction='in', length=3.5, which='minor')
ax.tick_params(right=True, left=True, top=True, bottom=True, which='both') #tick on all sides for both major and minor
ax.tick_params(axis='x', length=9, which='major')
ax.tick_params(axis='x', length=5, which='minor')


#https://stackoverflow.com/questions/20470892/how-to-place-minor-ticks-on-symlog-scale
class MinorSymLogLocator(Locator):
    """
    Dynamically find minor tick positions based on the positions of
    major ticks for a symlog scaling.
    """
    def __init__(self, linthresh):
        """
        Ticks will be placed between the major ticks.
        The placement is linear for x between -linthresh and linthresh,
        otherwise its logarithmically
        """
        self.linthresh = linthresh

    def __call__(self):
        'Return the locations of the ticks'
        majorlocs = self.axis.get_majorticklocs()

        # iterate through minor locs
        minorlocs = []

        # handle the lowest part
        for i in range(1, len(majorlocs)):
            majorstep = majorlocs[i] - majorlocs[i-1]
            if abs(majorlocs[i-1] + majorstep/2) < self.linthresh:
                ndivs = 10
            else:
                ndivs = 9
            minorstep = majorstep / ndivs
            locs = np.arange(majorlocs[i-1], majorlocs[i], minorstep)[1:]
            minorlocs.extend(locs)

        return self.raise_if_exceeds(np.array(minorlocs))

    def tick_values(self, vmin, vmax):
        raise NotImplementedError('Cannot get tick locations for a '
                                  '%s type.' % type(self))


a = gca()
a.set_xticklabels(a.get_xticks(), family='Arial')
a.set_yticklabels(a.get_yticks(), family='Arial')

plt.ylim([-10.1, 1000.1])
plt.xlim([-1.1, 6.87])


plt.yscale('symlog',linthresh=1e0)
yaxis = plt.gca().yaxis
yaxis.set_minor_locator(MinorSymLogLocator(1e0))


plt.xlabel('$P_{\mathtt{T}}$ [GeV]', horizontalalignment='right',x=1.0,family='Arial', fontsize=15)
plt.ylabel('Events / 0.5 GeV', horizontalalignment='right',y=1.0,family='Arial',fontsize=15 )
#aplt.atlas_label(0.2, 0.83, "Internal")

#atlas label
x_atlas_label = 0.17
y_atlas_label = 0.8
atlas_nexttext_distance = 0.11
atlas_energy_distance = 0.05
plt.text(x_atlas_label, y_atlas_label, 'ATLAS', fontfamily='Arial', fontstyle='italic', fontweight='bold',fontsize=14.3, transform=fig.transFigure)
plt.text(x_atlas_label + atlas_nexttext_distance , y_atlas_label, 'Internal', fontfamily='Arial',fontsize=14.3, transform=fig.transFigure)

#atlas energy label
plt.text(x_atlas_label, y_atlas_label - atlas_energy_distance, '$\mathrm{\sqrt{s}}$=13 TeV, 139 fb', fontfamily='Arial',fontsize=11, transform=fig.transFigure)
plt.text(x_atlas_label+0.202, y_atlas_label - atlas_energy_distance + 0.015, '-1', fontfamily='Arial',fontsize=7, transform=fig.transFigure)

plt.legend(loc="upper right",frameon=False, ncol=3, borderaxespad=2)

plt.savefig("symlogy4.png")
plt.savefig("symlogy4.pdf")
